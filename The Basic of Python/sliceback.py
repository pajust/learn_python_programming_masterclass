letters = "abcdefghijklmnopqrstuwxyz"

backwards = letters[::-1]
print(backwards)

# qpo
print(letters[16:13:-1])

# edbca
print(letters[4::-1])

# last 8 characters in reverse order
print(letters[:-9:-1])

# last 4 characters
print(letters[-4:])

# last character
print(letters[-1:])

# first character
print(letters[:1])
print(letters[0])