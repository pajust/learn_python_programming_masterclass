print("Today is a good day to lear Python")
print('Python is fun')
print("Python's string are easy to use")
print('We can event include "quotes" in strings')
print("hello" + " world")
greetings = "Hello"
name = "Tim"
print(greetings+name)

# if we want a space, we can add that too
print(greetings + ' ' + name)


age = 24
print(age)
print(type(greetings))
print(type(age))

age_in_words = "2 years"
print(name + f" is {age}  years old")
print(type(age_in_words))


print(f"Pi is approximately {22 / 7:12.50f}")
pi = 22 / 7
print(f"Pi is approximately {pi:12.50f}")