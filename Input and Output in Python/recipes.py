import shelve

bit = ["bacon", "tomato", "cheese", "bread"]
beans_and_toast = ["beans", "bread"]
scrambled_eggs = ["eggs", "butter", "milk"]
soup = ["tin of soup"]
pasta = ["pasta", "cheese"]

with shelve.open('recipes') as recipes:
    recipes["bit"] = bit
    recipes["beans_and_toast"] = beans_and_toast
    recipes["scrambled_eggs"] = scrambled_eggs
    recipes["soup"] = soup
    recipes["pasta"] = pasta
    recipes["soup"] = soup

    recipes["bit"].append("butter")
    recipes["pasta"].append("tomato")
    temp_list = recipes["bit"]
    temp_list.append("butter")
    recipes["bit"] = temp_list

    temp_list = recipes["pasta"]
    temp_list.append("tomato")
    recipes["pasta"] = temp_list

    for snack in recipes:
        print(snack, recipes[snack])
