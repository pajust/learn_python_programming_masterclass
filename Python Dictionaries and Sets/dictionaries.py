fruit = {"orange": "a sweet, orange, citrus fruit",
         "apple": "good for making cider",
         "lemon": "a sour, yellow citrus fruit",
         "grape": "a small, sweet fruit growing in bunches",
         "lime": "a sour, green citrus fruit"}

# while True:
#     dict_key = input("Please enter a fruit name: ")
#     if dict_key == "quit":
#         break
#     description = fruit.get(dict_key, "we don't have a " + dict_key)
#     print(description)

# ordered_keys = sorted(list(fruit.keys()))
# for f in ordered_keys:
#     print(f + " - " +fruit[f])
#
# for f in sorted(fruit.keys()):
#     print(f + " - " + fruit[f])
# fruit_keys = fruit.keys()
# print(fruit_keys)
#
# fruit["tomato"] = "not good with ice cream"
# print(fruit_keys)
print(fruit)
print(fruit.items())
f_tulpe = tuple(fruit.items())
print(f_tulpe)

for snack in f_tulpe:
    item, description = snack
    print(item + " is " + description)

print(dict(f_tulpe))