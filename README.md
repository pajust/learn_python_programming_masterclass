# README #

### What is this repository for? ###
* Learning Python Programming  
* Python 3.7.2

### Project structure: ###
* The Basic of Python
* Program Flow Control in Python
* Lists and Tuples
* Functions an Introduction
* Python Dictionaries and Sets
* Input and Output (I/O) in Python