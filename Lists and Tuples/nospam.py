menu = [
    ["egg", "bacon"],
    ["egg", "sausage", "bacon"],
    ["egg", "spam"],
    ["egg", "bacon", "spam"],
    ["egg", "bacon", "sausage", "spam"],
    ["spam", "bacon", "sausage", "spam"],
    ["spam", "sausage", "spam", "bacon", "spam", "tomato", "spam"],
    ["spam", "egg", "spam", "spam", "bacon", "spam"],
]

meal_to_remove = "spam"

for meal in menu:
    for index in range(len(meal) - 1, -1, -1):
        if meal[index] == meal_to_remove:
            del meal[index]
    print(", ".join(meal))
