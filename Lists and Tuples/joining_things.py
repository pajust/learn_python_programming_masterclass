flowers = [
    "Daffodil",
    "Evening Primrose",
    "Hydrangea",
    "Iris",
    "Sunflower",
    "Tiger Lily"
]

separator = " | "
output = separator.join(flowers)
print(output)