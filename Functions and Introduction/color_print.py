import colorama

# Some ANSI escape sequences for colours and effects
BLACK = '\u001b[30m'
RED = '\u001b[31m'
GREEN = '\u001b[32m'
YELLOW = '\u001b[33m'
BLUE = '\u001b[34m'
MAGENTA = '\u001b[35m'
CYAN = '\u001b[36m'
WHITE = '\u001b[37m'
RESET = '\u001b[0m'

BOLD = '\u001b[10m'
UNDERLINE = '\u001b[4m'
REVERSE = '\u001b[7m'


def color_print(text:  str, *effects: str) -> None:
    """
    Print `text` using the ANSI sequences to change color

    :param text: The text to print
    :param effects: The effect we want. One of constants
    defined at the start of this module.
    """
    effect_string = "".join(effects)
    output_string = "{0}{1}{2}".format(effect_string, text, RESET)
    print(output_string)


colorama.init()
color_print("Black text", BLACK)
color_print("Red text", RED)
color_print("Red bold text", RED, BOLD)
color_print("Green text", GREEN)
color_print("Yellow text", YELLOW)
color_print("Yellow reverse underlined text", YELLOW, REVERSE, UNDERLINE)
color_print("Blue reversed text", BLUE, REVERSE)
color_print("Magenta text", MAGENTA)
color_print("Cyan text", CYAN)
color_print("Bold text", BOLD)
color_print("Underline text", UNDERLINE)
color_print("Reversed text", REVERSE)
colorama.deinit()